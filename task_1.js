class Calc {
    constructor(number = 0) {
        if (!this._check_number(number)) {
            throw "Number is Not a number!"
        }

        this._result = number;
    }

    plus(number = 0) {
        if (!this._check_number(number)) {
            throw "Number is Not a number!"
        }

        this._result += number;

        return this;
    }

    minus(number = 0) {
        if (!this._check_number(number)) {
            throw "Number is Not a number!"
        }

        this._result -= number;

        return this;
    }

    res() {
        console.log(this._result);
        return this._result;
    }


    _check_number(number) {
        return Number.isFinite(number) && !Number.isNaN(number)
    }
}

